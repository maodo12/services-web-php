<?php
require_once("SimpleRest.php");
require_once("Aricle.php");

class ArticleRestHandler extends SimpleRest
{

	function getAllArticles($choix)
	{

		$article = new Article();
		$rawData = $article->getAllArticles();

		if (empty($rawData)) {
			$statusCode = 404;
			$rawData = array('success' => 0);
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$this->setHttpHeaders($requestContentType, $statusCode);

		$result["output"] = $rawData;

		switch ($choix) {

			case "json":
				if (strpos($requestContentType, 'application/json') !== false) {
					$response = $this->encodeJson($result);
					return $response;
				}
				break;

			case "xml":
				if (strpos($requestContentType, 'application/xml') !== false) {
					$response = $this->encodeXML($result);
					return $response;
				}
				break;
			default:
				$sms = 'choix inexistant';
				return $sms;
				break;
		}
	}

	function getArticlesByCategorie($choix)
	{

		$article = new Article();
		$rawData = $article->getArticlesByCategorie();

		if (empty($rawData)) {
			$statusCode = 404;
			$rawData = array('success' => 0);
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$this->setHttpHeaders($requestContentType, $statusCode);

		$result["output"] = $rawData;

		switch ($choix) {

			case "json":
				if (strpos($requestContentType, 'application/json') !== false) {
					$response = $this->encodeJson($result);
					return $response;
				}
				break;

			case "xml":
				if (strpos($requestContentType, 'application/xml') !== false) {
					$response = $this->encodeXML($result);
					return $response;
				}
				break;
			default:
				$sms = 'choix inexistant';
				return $sms;
				break;
		}
	}

	function getArticlesByCategorieX($categorie, $choix)
	{

		$article = new Article();
		$rawData = $article->getArticlesByCategorieX($categorie);

		if (empty($rawData)) {
			$statusCode = 404;
			$rawData = array('success' => 0);
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$this->setHttpHeaders($requestContentType, $statusCode);

		$result["output"] = $rawData;

		switch ($choix) {

			case "json":
				if (strpos($requestContentType, 'application/json') !== false) {
					$response = $this->encodeJson($result);
					return $response;
				}
				break;

			case "xml":
				if (strpos($requestContentType, 'application/xml') !== false) {
					$response = $this->encodeXML($result);
					return $response;
				}
				break;
			default:
				$sms = 'choix inexistant';
				return $sms;
				break;
		}
	}

	public function encodeJson($responseData)
	{
		$jsonResponse = json_encode($responseData);
		return $jsonResponse;
	}

	public function encodeXML($responseData)
	{
		$xmlResponse = xmlrpc_encode($responseData);
		return $xmlResponse;
	}
}
