<?php
require_once("dbcontroller.php");
/* 
A domain Class to demonstrate RESTful web services
*/
class Article
{
    private $articles = array();
    public function getAllArticles()
    {
        $query = 'SELECT * FROM article order by dateCreation desc';
        $dbcontroller = new DBController();
        $this->articles = $dbcontroller->executeSelectQuery($query);
        return $this->articles;
    }

    public function getArticlesByCategorie()
    {
        $query = 'SELECT id, titre, contenu, dateCreation, dateModification, categorie FROM article group by categorie';
        $dbcontroller = new DBController();
        $this->articles = $dbcontroller->executeSelectQuery($query);
        return $this->articles;
    }

    public function getArticlesByCategorieX($categorie)
    {
        $query = 'SELECT * FROM article categorie where categorie = ' . $categorie;
        $dbcontroller = new DBController();
        $this->articles = $dbcontroller->executeSelectQuery($query);
        return $this->articles;
    }
}
