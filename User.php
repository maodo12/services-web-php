<?php
require_once("dbcontroller.php");
/* 
A domain Class to demonstrate RESTful web services
*/
class Users
{
	private $users = array();
	public function getAllUsers()
	{
		$query = 'SELECT * FROM users';
		$dbcontroller = new DBController();
		$this->users = $dbcontroller->executeSelectQuery($query);
		return $this->users;
	}

	public function addUser($username, $email, $user_type, $password)
	{
		$query = "insert into users (username, email, user_type, password) values ('" . $username . "','" . $email . "','" . $user_type . "','" . $password . "')";
		$dbcontroller = new DBController();
		$result = $dbcontroller->executeQuery($query);
		if ($result != 0) {
			$result = array('success' => 1);
			return $result;
		}
	}

	public function deleteUser($id)
	{
		$query = "DELETE FROM users WHERE id = " . $id;
		$dbcontroller = new DBController();
		$result = $dbcontroller->executeQuery($query);
		if ($result != 0) {
			$result = array('success' => 1);
			return $result;
		}
	}


	public function editUser($id, $username, $email, $user_type, $password)
	{
		$query = "UPDATE users SET username = '" . $username . "',email ='" . $email . "',user_type = '" . $user_type . "',password = '" . $password . "' WHERE id = " . $id;
		$dbcontroller = new DBController();
		$result = $dbcontroller->executeQuery($query);
		if ($result != 0) {
			$result = array('success' => 1);
			return $result;
		}
	}

	public function login($login, $password)
	{

		$query = "SELECT id, username, password FROM users WHERE username='" . $login . "'  
			and password='" . $password . "'";

		$dbcontroller = new DBController();
		$result = $dbcontroller->executeSelectLogin($query);

		$userId = "";
		while ($r = mysqli_fetch_row($result)) {
			$userId = $r[0];
		}

		if ($result->num_rows > 0) {

			$resp["status"] = "1";
			$resp["userid"] = $userId;
			$resp["message"] = "Login successfully";
		} else {
			$resp["status"] = "-2";
			$resp["message"] = "Enter correct username or password";
		}

		header('content-type: application/json');
		$response["response"] = $resp;
		return json_encode($response);
	}
}
