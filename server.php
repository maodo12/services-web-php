<?php
require 'lib/nusoap.php';
$server=new nusoap_server();
$server->configureWSDL("administration_users", "urn:administration_users");
$server->register(
    "getAllUsers",
    array(),
    array("return"=>"xsd:string[]")
);
$server->register(
    "addUser",
    array("username"=>"xsd:string", "email"=>"xsd:string",
     "user_type"=>"xsd:string", "password"=>"xsd:string"),
    array("return"=>"xsd:integer")
);
$server->register(
    "deleteUser",
    array("id"=>"xsd:integer"),
    array("return"=>"xsd:integer")
);
$server->register(
    "editUser",
    array("id"=>"xsd:integer", "username"=>"xsd:string", "email"=>"xsd:string",
     "user_type"=>"xsd:string", "password"=>"xsd:string"),
    array("return"=>"xsd:integer")
);
$server->register(
    "login",
    array("username"=>"xsd:string", "password"=>"xsd:string"),
    array("return"=>"xsd:string")
);
$server->register(
    "getAllArticles",
    array("choix"=>"xsd:string"),
    array("return"=>"xsd:string[]")
);
$server->register(
    "getArticlesByCategorie",
    array("choix"=>"xsd:string"),
    array("return"=>"xsd:string[]")
);
$server->register(
    "getArticlesByCategorieX",
    array("categorie"=>"xsd:categorie", "choix"=>"xsd:string"),
    array("return"=>"xsd:string[]")
);

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
